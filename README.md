Test Application to parse pictures.
========


Project use:
- Symfony 3.1.1
- Twitter Bootstrap

## How to install ##

1. clone this project
2. go to root project folder
3. Run command: **composer install && bower install**

4. Run **php bin/symfony_requirements**
5. Configure config file: **app/config/parameters.yml**
6. Create database: **php bin/console doctrine:database:create**
7. Create tables: **php console doctrine:schema:update --force**
8. Run server: **php bin/console server:run**



I tested parser on:
* https://www.amazon.com/
* https://www.onliner.by/
* http://www.tut.by/
* http://www.ebay.com/