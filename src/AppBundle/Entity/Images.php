<?php
/**
 * Created by PhpStorm.
 * User: anatoly
 * Date: 21.6.16
 * Time: 11.09
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="images")
 */
class Images
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Site
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Site", inversedBy="images")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $site;

    /**
     * @var string
     * @ORM\Column(name="img", type="string")
     */
    protected $img;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param Site $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

}