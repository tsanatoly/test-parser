<?php
/**
 * Created by PhpStorm.
 * User: anatoly
 * Date: 21.6.16
 * Time: 9.38
 */

namespace AppBundle\Service;

use AppBundle\Entity\Images;
use AppBundle\Entity\Site;
use Symfony\Component\DomCrawler\Crawler;

class Parser
{
    protected $url;
    protected $folder;
    protected $siteFolder;
    protected $html;

    /**
     * @var $em \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct($em, $rootDir)
    {
        $this->em = $em;
        $this->folder = str_replace('app', 'web/uploads/', $rootDir);
    }
    
    public function setUrl($url) {
        $this->url = $url;
    }

    public function process()
    {

        $site = $this->em->getRepository('AppBundle\Entity\Site')->findOneByUrl($this->url);
        if (!$site) {
            $site = new Site();
            $site->setUrl($this->url);
            $site->setTitle($this->getSiteName());
            $this->em->persist($site);
            $this->em->flush();
        }

        $aImg = $this->_parse();
        $aImg = $this->_save($aImg);

        foreach ($aImg as $img) {
            $image = $this->em->getRepository('AppBundle\Entity\Images')->findOneBy(['img' => basename($img), 'site' => $site->getId()]);
            if (!$image) {
                $image = new Images();
            }
            $image->setImg(basename($img));
            $image->setSite($site);
            $this->em->persist($image);
        }

        $this->em->flush();

        return true;
    }

    public function getSiteName()
    {
        return parse_url($this->url)['host'];
    }

    public function getSiteFolder()
    {
        if (!$this->siteFolder) {
            $this->siteFolder = $this->_setFolder();
        }

        return $this->siteFolder;
    }

    private function _setFolder()
    {
        $siteDir = $this->folder . $this->getSiteName();
        if (!file_exists($siteDir)) {
            mkdir($siteDir);
        }
        return $siteDir;
    }

    private function _parse()
    {
        $html =file_get_contents($this->url);
        $crawler = new Crawler($html);
        $result = $crawler
            ->filterXpath('//img')
            ->extract(array('src'));

        return array_filter($result);
    }

    private function _save($img)
    {
        $aImg = [];
        $aUrl = parse_url($this->url);
        $url = $aUrl['scheme'] . '://' . $aUrl['host'];

        foreach ($img as $innerImage) {
            $image = $this->_url($innerImage);

            if (!$image) continue;


            $path = $this->getSiteFolder()."/".basename($image);
            $file = file_get_contents($image);

            $insert = file_put_contents($path, $file);

            $aImg[] = $innerImage;

            if (!$insert) {
                throw new \Exception('Failed to write image');
            }
        }

        return $aImg;

    }

    private function _url($url)
    {
        // check extension
        if (!preg_match('/(\.jpg|\.png|\.bmp)$/', $url)) {
            return false;
        }


        // check if image placed on subdomain
        if ((count(parse_url($url)) > 1) && ($this->getSiteName() != parse_url($url)['host'])) {
            return $url;
        }

        $aSite = parse_url($this->url);
        $homeUrl = $aSite['scheme'] . '://' . $aSite['host'];

        return $homeUrl . $url;
    }


}