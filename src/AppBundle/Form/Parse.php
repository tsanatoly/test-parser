<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;



class Parse extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', UrlType::class, [
                'label' => 'Insert parse link:',
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => "Field cannot be empty"]),
                    new Length(['min' => 5]),
                ],
                'attr' => [
                    'class' => 'form-control ',
                    'id' => 'url',
                    'placeholder' => 'url',
                ]
            ])
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'parse';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
//            'data_class'      => 'AppBundle\Entity\Task',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'parse_site',
        ));
    }
}