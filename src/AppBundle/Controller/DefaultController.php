<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Images;
use AppBundle\Entity\Site;
use AppBundle\Form\Parse;
use AppBundle\Service\Parser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(Parse::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            /** @var $parser \AppBundle\Service\Parser */
            $parser = $this->get('parser');
            $parser->setUrl($data['url']);
            $parser->process();
        }


        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'sites' => $this->getDoctrine()->getRepository('AppBundle\Entity\Site')->findAll(),
        ]);
    }


    /**
     * @Route("/image/{id}", name="images")
     * @ParamConverter("site", class="AppBundle:Site")
     */
    public function ImagesAction(Site $site)
    {
        $images = $this->getDoctrine()->getRepository('AppBundle\Entity\Images')->findBySite($site);

        return $this->render('default/images.html.twig', [
            'images' => $images,
            'site' => $site,
        ]);
    }
}
